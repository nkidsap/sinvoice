﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService
{
   public static  class FileHelper
    {
        public static void CreateFile(string type, string filename,string fileToBytes)
        {
            try
            {
                // Specifing a file name 
                var path = GetTemporaryDirectory()+ @"/"+ filename+"."+type;
                // Specifing a byte array 
                 byte[] bytes = Convert.FromBase64String(fileToBytes);

                // Calling the WriteAllBytes() function 
                // to write specified byte array to the file 
                System.IO.File.WriteAllBytes(path, bytes);
                Process.Start(path);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static string GetTemporaryDirectory()
        {
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);
            return tempDirectory;
        }
    }
}
