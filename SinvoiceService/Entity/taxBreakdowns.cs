﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class taxBreakdowns
    {
        public decimal? taxPercentage { get; set; }
        public decimal? taxableAmount { get; set; }
        public decimal? taxAmount { get; set; }
        public decimal? taxableAmountPos { get; set; }
        public decimal? taxAmountPos { get; set; }
        public string taxExemptionReason { get; set; }
    }
}
