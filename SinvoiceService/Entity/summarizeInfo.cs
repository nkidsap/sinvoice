﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class summarizeInfo
    {
        public decimal? sumOfTotalLineAmountWithoutTax { get; set; }
        public decimal? totalAmountWithoutTax { get; set; }
        public decimal? totalTaxAmount { get; set; }
        public decimal? totalAmountWithTax { get; set; }
        public decimal? totalAmountWithTaxFrn { get; set; }
        public decimal? discountAmount { get; set; }
       
        public string totalAmountWithTaxInWords { get; set; }

        public bool isTotalAmountPos { get; set; }
        public bool isTotalTaxAmountPos { get; set; }
        public bool isTotalAmtWithoutTaxPos { get; set; }
        public decimal? totalAmountAfterDiscount { get; set; }
        public decimal? settlementDiscountAmount { get; set; }
        public bool isDiscountAmtPos { get; set; }
    }
}
