﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SInvoiceService.Entity
{
    public class AdditionalReference
    {
        public string additionalReferenceDesc { get; set; }
        public string additionalReferenceDate { get; set; }
    }
}
