﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class invoice
    {
        public sellerInfo sellerInfo { get; set; } = new sellerInfo();
        public buyerInfo buyerInfo { get; set; } = new buyerInfo();

        public generalInvoiceInfo generalInvoiceInfo { get; set; } = new generalInvoiceInfo();
        public deliveryInfo deliveryInfo { get; set; } = new deliveryInfo();
        public List<itemInfo> itemInfo { get; set; } = new List<itemInfo>();
        public List<payments> payments { get; set; } = new List<payments>();
        public List<taxBreakdowns> taxBreakdowns { get; set; } = new List<taxBreakdowns>();
        public summarizeInfo summarizeInfo { get; set; } = new summarizeInfo();
        public List<metadata> metadata { get; set; } = new List<metadata>();
        
    }
    public enum TransType
    {
        ARInvoice = 13,
        ARCreditMemo = 14,
        Transfer = 67
    }
}
