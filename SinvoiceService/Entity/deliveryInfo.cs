﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class deliveryInfo
    {
        public string deliveryOrderNumber { get; set; }
        public long deliveryOrderDate { get; set; }
        public string deliveryOrderBy { get; set; }
        public string deliveryBy { get; set; }
        public string fromWarehouseName { get; set; }
        public string toWarehouseName { get; set; }
        public string transportationMethod { get; set; }
        public string containerNumber { get; set; }
        public string deliveryOrderContent { get; set; }
    }
}
