﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class ResultDoc
    {

        public string errorCode { get; set; }

        public string description { get; set; }

        public result result { get; set; } = new result();

    }
    public class result
    {
        public string invoiceNo { get; set; }
        public string transactionID { get; set; }
        public string reservationCode { get; set; }
    }
}
