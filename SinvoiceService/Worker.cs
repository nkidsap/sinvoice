﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SInvoiceService.Entity;
using SInvoiceService.Class;
using Newtonsoft.Json;

namespace SInvoiceService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private IServiceScopeFactory _serviceScopeFactory;

        //private IList<string> _folderPaths;
        //private int _numberOfDaysBeforeDelete;
        private int _runIntervallInMinutes;
        private string _dbconnectionstring;
        private string _baseurl;
        private string _username;
        private string _password;


        
        private string _host;
        private int _port;
        private string _displayname;
        private string _mailfromaddress;
        private string _mailtoaddress;
        private string _mailfrompassword;
        private string _mailsubject;
        public Worker(ILogger<Worker> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {

            //string encryptedUserName = EncryptionHelper.Encrypt("Data Source=210.211.116.19;Initial Catalog=SAP_NKID_CORP;User ID=support02;PWD=Nkid@Sap");



            var configuration = _serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<IConfiguration>();

         


            _runIntervallInMinutes = int.Parse(configuration["App.Configurations:RunIntervallInMinutes"]);
   
            _dbconnectionstring = EncryptionHelper.Decrypt(configuration["App.Configurations:DbConnection"]);
            _baseurl = configuration["App.Configurations:BaseUrl"];
            _username = configuration["App.Configurations:Username"];
            _password = EncryptionHelper.Decrypt(configuration["App.Configurations:Password"]);

            _port = int.Parse(EncryptionHelper.Decrypt(configuration["MailConfigurations:Port"]));
            _host = EncryptionHelper.Decrypt(configuration["MailConfigurations:Host"]);
            _displayname = configuration["MailConfigurations:DisplayName"];
            _mailfromaddress = configuration["MailConfigurations:MailFromAddress"];
            _mailtoaddress = configuration["MailConfigurations:MailToAddress"];
            _mailfrompassword = EncryptionHelper.Decrypt(configuration["MailConfigurations:MailFromPassword"]);
            _mailsubject = configuration["MailConfigurations:MailSubject"];

            
            BaseData.SetDBConnection(_dbconnectionstring);
            MailHelper.SettingMail(new Mail() { Host = _host, MailDisplay = _displayname, Port = _port, OpenSSL = true, MailFromAddress = _mailfromaddress, MailFromPassword = _mailfrompassword });
            InvoiceManagement.SetApiAuthentication(_baseurl, _username, _password);

            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now.AddHours(7));

                //DO SOMETHING
                await PublishInvoices();
               
                await Task.Delay(TimeSpan.FromMinutes(_runIntervallInMinutes), stoppingToken);
            }
        }
        private async Task PublishInvoices()
        {
            //get list invoice publish
            List<DocPublish> lsDoc = await BaseData.GetListDocPublish(DateTime.Now.Date.AddDays(-7), DateTime.Now.Date, _logger);
            
            foreach (var doc in lsDoc)
            {
                if (doc.IsCanceled == false)
                {
                    if (doc.TransType != TransType.ARCreditMemo)
                    {
                        await PublishInvoice(doc.DocNo, doc.TransType);
                        Thread.Sleep(3000);
                    }

                    if (doc.TransType == TransType.ARCreditMemo)
                    {
                        await AdjustInvoice(doc.DocNo, "2", doc.TransType, false);
                        Thread.Sleep(3000);
                    }
                }
                else
                {
                    await CancelInvoicesDocEntry(doc.DocNo);
                    Thread.Sleep(3000);
                }
            }

        }
       
        private async Task PublishInvoice(string _invno, TransType _transtype)
        {

            invoice inv = await BaseData.GetInvoiceInfo(_invno, _transtype);
            inv.generalInvoiceInfo.invoiceIssuedDate = inv.generalInvoiceInfo.IssuedDate.ToString("yyyy-MM-ddTHH:mm:ss.fff+0700");// "2020-10-08T20:20:00.000+0700"; // UnixTimeHelper.ToUnixTime(inv.generalInvoiceInfo.IssuedDate);
            sinvoicelog log = await InvoiceManagement.PublishInvoice(inv);
            log.docentry = _invno;
            log.templatecode = inv.generalInvoiceInfo.templateCode;
            log.serial = inv.generalInvoiceInfo.invoiceSeries;
            if (_transtype == TransType.ARInvoice)
                log.transtype = "OINV";
            if (_transtype == TransType.ARCreditMemo)
                log.transtype = "ORIN";
            if (_transtype == TransType.Transfer)
                log.transtype = "OWTR";
            //Lay danh sach Invoice da sync
            Thread.Sleep(3000);
            List<inv> rs = await InvoiceManagement.FindInvoice(log.supplierTaxCode,
                DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd"),
                DateTime.Now.ToString("yyyy-MM-dd"),
                inv.generalInvoiceInfo.invoiceSeries,
                1,
                1,
                inv.generalInvoiceInfo.templateCode,
                 log.invoiceNo
                );

            //var ls = (from x in rs
            //          where x.invoiceNo == log.invoiceNo
            //          select x).FirstOrDefault<inv>();

            if (rs != null && log.invoiceNo != null)
            {
                log.issuedate_unixtime = rs.FirstOrDefault<inv>().issueDate.ToString();
                log.issueDate = UnixTimeHelper.ToDateTime(rs.FirstOrDefault<inv>().issueDate);
            }
            else
            {
                log.issuedate_unixtime = null;
                log.issueDate = DateTime.Now;
            }
            log.templatecode = inv.generalInvoiceInfo.templateCode;
            log.serial = inv.generalInvoiceInfo.invoiceSeries;
            log.paymenttype = inv.payments.FirstOrDefault().paymentMethodName;
            await BaseData.SaveSinvoiceLog(log, "publish");
            
            string msg = "";
            if (log.errorCode is null)
            {
                msg = string.Format("Success Publish " + log.transtype + " " + log.docentry + " - InvoiceNo: " + log.invoiceNo + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                _logger.LogInformation(msg);
            }
            else
            {
                msg = string.Format("Error Publish " + log.transtype + " " + log.docentry + " - " + log.description + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                _logger.LogError(msg);
            }
            //write file json 
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"/Log"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"/Log");
            string filename = AppDomain.CurrentDomain.BaseDirectory+@"/Log/"+ log.docentry + ".json";
            if (File.Exists(filename))
                File.Delete(filename);
            File.WriteAllText(filename, log.objectjson);

            await MailHelper.SendMail(new MailObject() { ToAddress = _mailtoaddress, Body = msg, Subject = _mailsubject, Attachments=filename });

           
        }
        private async Task AdjustInvoice(string _invno, string adjustmentInvoiceType, TransType _transtype, bool isIncreaseItem = true)
        {
            invoice inv = await BaseData.GetInvoiceInfo(_invno, _transtype);

            //invoice inv = await BaseData.GetARCreditAdjustInfo(_invno);


            sinvoicelog log = await InvoiceManagement.AdjustInvoice(inv, adjustmentInvoiceType, isIncreaseItem);
            log.docentry = _invno;
            log.originalinvoiceNo = inv.generalInvoiceInfo.originalInvoiceId;
            log.templatecode = inv.generalInvoiceInfo.templateCode;
            log.serial = inv.generalInvoiceInfo.invoiceSeries;
            log.transtype = "ORIN";
            //Lay danh sach Invoice da sync
            Thread.Sleep(3000);
            List<inv> rs = await InvoiceManagement.FindInvoice(log.supplierTaxCode,
                DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd"),
                DateTime.Now.ToString("yyyy-MM-dd"),
                inv.generalInvoiceInfo.invoiceSeries,
                1,
                1,
                inv.generalInvoiceInfo.templateCode,
                log.invoiceNo
                );

            //var ls = (from x in rs
            //          where x.invoiceNo == log.invoiceNo
            //          select x).FirstOrDefault<inv>();
            Thread.Sleep(3000);
            if (rs != null && log.invoiceNo != null)
            {
                log.issuedate_unixtime = rs.FirstOrDefault<inv>().issueDate.ToString();
                log.issueDate = UnixTimeHelper.ToDateTime(rs.FirstOrDefault<inv>().issueDate);
            }
            else
            {
                log.issuedate_unixtime = null;
                log.issueDate = DateTime.Now;
            }
            log.templatecode = inv.generalInvoiceInfo.templateCode;
            log.serial = inv.generalInvoiceInfo.invoiceSeries;
            log.paymenttype = inv.payments.FirstOrDefault().paymentMethodName;
            await BaseData.SaveSinvoiceLog(log, "publish");
          
            string msg = "";
            if (log.errorCode is null)
            {
                msg = string.Format("Success Publish Adjust " + log.transtype + " " + log.docentry + " - InvoiceNo: " + log.invoiceNo + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                _logger.LogInformation(msg);
            }    
               
            else
            {
                msg = string.Format("Error Publish Adjust " + log.transtype + " " + log.docentry + " - " + log.description + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                _logger.LogError(msg);
            }
            //write file json 
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"/Log"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"/Log");
            string filename = AppDomain.CurrentDomain.BaseDirectory + @"/Log/" + log.docentry + ".json";
            if (File.Exists(filename))
                File.Delete(filename);
            File.WriteAllText(filename, log.objectjson);

            await MailHelper.SendMail(new MailObject() { ToAddress = _mailtoaddress, Body = msg, Subject = _mailsubject, Attachments = filename });
        }
        private async Task CancelInvoicesDocEntry(string docentry)
        {
            sinvoicelog invlog = await BaseData.GetSInvoiceInfo(docentry);

            AdditionalReference addref = await BaseData.GetSInvoiceAdditionalReference(invlog.invoiceNo);

            if (addref.additionalReferenceDesc != "#")
            {
                sinvoicelog log = await InvoiceManagement.CancelInvoice(invlog.supplierTaxCode, invlog.invoiceNo, invlog.issueDate.ToString("yyyyMMdd000000"), addref.additionalReferenceDesc, addref.additionalReferenceDate);
                Thread.Sleep(3000);
                if (log.errorCode == null)
                    invlog.iscanceled = true;

                invlog.objectjson = log.objectjson;
                invlog.errorCode = log.errorCode;
                invlog.description = log.description;
                invlog.isupdatetax = false;
                invlog.ispayment = false;

                await BaseData.SaveSinvoiceLog(invlog, "cancel");
                string msg = "";
                if (invlog.errorCode is null)
                {
                    msg = string.Format("Success Cancel Document " + invlog.docentry + " - InvoiceNo: " + invlog.invoiceNo + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                    _logger.LogInformation(msg);
                }

                else
                {
                    msg = string.Format("Error Cancel Document "  + invlog.docentry + " - " + invlog.description + " Time: {0}", DateTimeOffset.Now.AddHours(7));
                    _logger.LogError(msg);
                }
                await MailHelper.SendMail(new MailObject() { ToAddress = _mailtoaddress, Body = msg, Subject = _mailsubject });
            }

        }


    }
}
