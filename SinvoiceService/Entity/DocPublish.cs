﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SInvoiceService.Entity
{
    public class DocPublish
    {
        public string DocNo { get; set; }
        public DateTime? DocDate { get; set; }
        public TransType TransType { get; set; }
        public bool IsCanceled { get; set; }
    }
}
