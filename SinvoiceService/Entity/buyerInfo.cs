﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public  class buyerInfo
    {
       
        public string buyerName { get; set; }
       
        public string buyerCode { get; set; }
        public string buyerLegalName { get; set; }
       
        public string buyerTaxCode { get; set; }
       
        public string buyerAddressLine { get; set; }
        public string buyerPhoneNumber { get; set; }
      
        public string buyerFaxNumber { get; set; }
        public string buyerEmail { get; set; }
        public string buyerBankName { get; set; }
        public string buyerBankAccount { get; set; }
        public string buyerDistrictName { get; set; }

        public string buyerCityName { get; set; }
        public string buyerCountryCode { get; set; }

        public string buyerIdType { get; set; }

        public string buyerIdNo { get; set; }

        public DateTime? buyerBirthDay { get; set; }
    }
}
