﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class generalInvoiceInfo
    {
        public string invoiceType { get; set; }
        public string invoiceNo { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeries { get; set; }
        [JsonIgnore]
        public DateTime IssuedDate { get; set; }

        public string invoiceIssuedDate { get; set; }
        public string currencyCode { get; set; }
        public string adjustmentType { get; set; }
        public string adjustmentInvoiceType { get; set; }
        public string originalInvoiceId { get; set; }


        public long? originalInvoiceIssueDate { get; set; }

        public string additionalReferenceDesc { get; set; }

        public long? additionalReferenceDate { get; set; }

        public bool paymentStatus { get; set; } = true;
        public bool cusGetInvoiceRight { get; set; } = true;

        public decimal? exchangeRate { get; set; }
        public string transactionUuid { get; set; }
        public string userName { get; set; }
        public string certificateSerial { get; set; }

    }
}
