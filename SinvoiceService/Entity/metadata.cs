﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
   public  class metadata
    {
        public string keyTag { get; set; }
        public string valueType { get; set; }
        public DateTime? dateValue { get; set; }
        public string stringValue { get; set; }
        public int numberValue { get; set; }
        public string keyLabel { get; set; }
        public bool isRequired { get; set; }
        public bool isSeller { get; set; }
    }
}
