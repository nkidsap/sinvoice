﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class itemInfo
    {
        public int? lineNumber { get; set; }
        public int selection { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public decimal? unitPrice { get; set; }
        public decimal? quantity { get; set; }
        public decimal? itemTotalAmountWithoutTax { get; set; }

        public decimal? taxPercentage { get; set; }
        public decimal? taxAmount { get; set; }
        public decimal? adjustmentTaxAmount { get; set; } = 0;
        public bool? isIncreaseItem { get; set; } 
        public string itemNote { get; set; }
        public string batchNo { get; set; }
        public string expDate { get; set; }
        public decimal? discount { get; set; }
        public decimal? discount2 { get; set; }
        public decimal? itemDiscount { get; set; }
        public decimal? itemTotalAmountAfterDiscount { get; set; }
        public decimal? itemTotalAmountWithTax { get; set; }
    }
    
}
