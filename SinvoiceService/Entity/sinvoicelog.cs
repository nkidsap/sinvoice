﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public class sinvoicelog
    {
        public string docentry { get; set; }
        public DateTime syncdate { get; set; }
        public string errorCode { get; set; }
        public string description { get; set; }
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        public string originalinvoiceNo { get; set; }
        public string transactionID { get; set; }
        public string reservationCode { get; set; }
        public string objectjson { get; set; }
        public string transtype { get; set; }
        public string templatecode { get; set; }
        public string serial { get; set; }
        public string issuedate_unixtime { get; set; }
        public DateTime issueDate { get; set; }
        public bool? iscanceled { get; set; } = false;
        public bool? isupdatetax { get; set; } = false;
        public string paymenttype { get; set; }
        public bool? ispayment { get; set; } = false;
    }
}
