FROM mcr.microsoft.com/dotnet/core/sdk:3.1-bionic
COPY SinvoiceService/bin/Release/netcoreapp3.1/publish/ App/
WORKDIR /App
ENTRYPOINT ["dotnet", "SInvoiceService.dll"]