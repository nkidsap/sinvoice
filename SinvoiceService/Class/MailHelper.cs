﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Class
{
    public static class MailHelper
    {
        #region "Parameter"


        private static string Host { get; set; }

        private static int Port { get; set; }

        private static string MailDisplay { get; set; }

        private static string MailFromAddress { get; set; }

        private static string MailFromPassword { get; set; }

        private static bool OpenSSL { get; set; }

        #endregion
        public static void SettingMail(Mail mail)
        {
            Host = mail.Host;
            Port = mail.Port;
            MailFromAddress = mail.MailFromAddress;
            MailFromPassword = mail.MailFromPassword;
            MailDisplay = mail.MailDisplay;
            OpenSSL = mail.OpenSSL;
        }
        public static async Task<string> SendMail(MailObject _Mail)
        {
            try
            {

                SmtpClient smtpclient = new SmtpClient(Host, Int32.Parse(Port.ToString()));

                smtpclient.EnableSsl = OpenSSL;

                MailAddress From = new MailAddress(MailFromAddress, MailDisplay);
                MailMessage Mail = new MailMessage();
                Mail.From = From;
                
                if (_Mail.ToAddress != null)
                {
                    if (_Mail.ToAddress.Contains(";"))
                    {
                        foreach (string _to in _Mail.ToAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (_to.Length > 0)
                                Mail.To.Add(_to);
                        }

                    }
                    else
                    {
                        if (_Mail.ToAddress.Length > 0)
                            Mail.To.Add(_Mail.ToAddress);
                    }

                }

                if (_Mail.BCCAddress != null)
                {
                    if (_Mail.BCCAddress.Contains(";"))
                    {
                        foreach (string _BCC in _Mail.BCCAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (_BCC.Length > 0)
                                Mail.Bcc.Add(_BCC);
                        }

                    }
                    else
                    {
                        if (_Mail.BCCAddress.Length > 0)
                            Mail.Bcc.Add(_Mail.BCCAddress);
                    }

                }


                if (_Mail.CCAddress != null)
                {
                    if (_Mail.CCAddress.Contains(";"))
                    {
                        foreach (string _CC in _Mail.CCAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (_CC.Length > 0)
                                Mail.CC.Add(_CC);
                        }

                    }
                    else
                    {
                        if (_Mail.CCAddress.Length > 0)
                            Mail.CC.Add(_Mail.CCAddress);
                    }

                }
                if (_Mail.Attachments != null)
                {
                    if (_Mail.Attachments.Contains(";"))
                    {
                        foreach (string _att in _Mail.Attachments.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (_att.Length > 0)
                                Mail.Attachments.Add(new Attachment(_att));
                        }

                    }
                    else
                    {
                        if (_Mail.Attachments.Length > 0)
                            Mail.Attachments.Add(new Attachment(_Mail.Attachments));
                    }
                }    
                Mail.IsBodyHtml = true;
                Mail.BodyEncoding = System.Text.Encoding.UTF8;
                Mail.SubjectEncoding = System.Text.Encoding.UTF8;
                Mail.Priority = MailPriority.High;
                Mail.Body = _Mail.Body+ Environment.NewLine;
                Mail.Subject = _Mail.Subject;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = MailFromAddress;
                credentials.Password = MailFromPassword;
                smtpclient.Credentials = credentials;
                // smtpclient.UseDefaultCredentials = True
                await smtpclient.SendMailAsync(Mail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

    }
    public class MailObject
    {
       
        public string ToAddress { get; set; }

        public string CCAddress { get; set; }

        public string BCCAddress { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
        public string Attachments { get; set; }
    }
    public class Mail
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public string MailDisplay { get; set; }

        public string MailFromAddress { get; set; }

        public string MailFromPassword { get; set; }

        public bool OpenSSL { get; set; }
    }
}
