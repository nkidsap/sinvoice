﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public   class ResultFile
    {
        
        public string errorCode { get; set; }

        public string description { get; set; }
       
        public string fileName { get; set; }

        public string fileToBytes { get; set; }
        public bool? paymentStatus { get; set; }
        
    }
}
