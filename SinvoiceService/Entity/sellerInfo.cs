﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService.Entity
{
    public  class sellerInfo
    {
        //Tên (đăng ký kinh doanh trong trường hợp là doanh nghiệp) của người bán
        [Required]
        public  string sellerLegalName { get; set; }
        //Mã số thuế người bán được cấp bởi TCT Việt Nam. 
        [Required]
        public  string sellerTaxCode { get; set; }
        public  string sellerCode { get; set; }
        //Địa chỉ của bên bán được thể hiện trên hóa đơn.
        [Required]
        public  string sellerAddressLine { get; set; }
       
        public  string sellerPhoneNumber { get; set; }
        public  string sellerFaxNumber { get; set; }
      
        public  string sellerEmail { get; set; }
        public  string sellerBankName { get; set; }
        public  string sellerBankAccount { get; set; }
        public  string sellerDistrictName { get; set; }
        public  string sellerCityName { get; set; }

        public  string sellerCountryCode { get; set; }
        public  string sellerWebsite { get; set; }
    }
}
