﻿using Dapper;
using Microsoft.Extensions.Logging;
using SInvoiceService.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvoiceService
{
    public class BaseData
    {
        private static string connectstring = "";
        
        public static void SetDBConnection(string _connectstring)
        {
            connectstring = _connectstring;
        }
        public static async Task<invoice> GetInvoiceInfo(string DocEntry, TransType TransType, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_INVOICE_INFO",
                    new { @DocEntry = DocEntry, @TransType = TransType },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                if (TransType != TransType.Transfer)
                {
                    try
                    {
                        List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                        foreach (itemInfo it in its_discount)
                        {
                            its.Add(it);
                        }
                    }
                    catch { }

                }
                deliveryInfo deli = new deliveryInfo();
                if (TransType == TransType.Transfer)
                {
                    try
                    {
                        deli = result.Read<deliveryInfo>().FirstOrDefault();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum,
                    deliveryInfo = deli
                };


                return inv;



            }

        }

        public static async Task<invoice> GetARCreditInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<invoice> GetARCreditAdjustInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "[USP_SINVOICE_GET_ORIN_ADJUST_INFO]",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_discount = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_discount)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<invoice> GetAdjustInvoiceInfo(string DocEntry, string adjustmentType = "1")
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_ORIN_INFO",
                    new { @DocEntry = DocEntry },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                sellerInfo sel = result.Read<sellerInfo>().FirstOrDefault();

                generalInvoiceInfo gen = result.Read<generalInvoiceInfo>().FirstOrDefault();

                buyerInfo buy = result.Read<buyerInfo>().FirstOrDefault();
                List<payments> pms = result.Read<payments>().ToList();
                summarizeInfo sum = result.Read<summarizeInfo>().FirstOrDefault();

                List<taxBreakdowns> taxs = result.Read<taxBreakdowns>().ToList();
                List<itemInfo> its = result.Read<itemInfo>().ToList();

                //item info discount header
                List<itemInfo> its_adjust = result.Read<itemInfo>().ToList();
                foreach (itemInfo it in its_adjust)
                {
                    its.Add(it);
                }
                //if (sum.discountAmount!=0)
                //{
                //    its.Add(new itemInfo() { lineNumber=its.Count+1, selection = 3,itemCode="chiet_khau_hang_hoa",itemName="Chiết khấu hàng hóa",taxAmount=0,discount=0,taxPercentage=0,itemDiscount=0, isIncreaseItem = false, itemTotalAmountWithoutTax = sum.discountAmount, itemTotalAmountWithTax = sum.discountAmount });
                //}
                invoice inv = new invoice()
                {
                    sellerInfo = sel,
                    buyerInfo = buy,
                    generalInvoiceInfo = gen,
                    payments = pms,
                    itemInfo = its,
                    taxBreakdowns = taxs,
                    summarizeInfo = sum
                };


                return inv;



            }

        }

        public static async Task<sinvoicelog> GetSInvoiceInfo(string DocEntry)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where DocEntry={0}   and invoiceNo is not null", DocEntry),
                    null,
                    null, Int32.MaxValue, CommandType.Text);

                sinvoicelog log = result.Read<sinvoicelog>().FirstOrDefault();


                return log;



            }

        }
        public static async Task<AdditionalReference> GetSInvoiceAdditionalReference(string InvoiceNo)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    "USP_SINVOICE_GET_ADDITIONALREFERENCE",
                    new { @INVOICENO = InvoiceNo },
                    null, Int32.MaxValue, CommandType.StoredProcedure);

                AdditionalReference log = result.Read<AdditionalReference>().FirstOrDefault();


                return log;



            }

        }
        public static async Task<sinvoicelog> GetSInvoiceInfoByInvNo(string InvoiceNo)
        {
            // Act
            using (var conn = new SqlConnection(connectstring))
            {
                await conn.OpenAsync();
                var result = await conn.QueryMultipleAsync(
                    string.Format("select top 1 * from TVT_SINVOICE_LOG (nolock) where invoiceNo='{0}'  and invoiceNo is not null", InvoiceNo),
                    null,
                    null, Int32.MaxValue, CommandType.Text);

                sinvoicelog log = result.Read<sinvoicelog>().FirstOrDefault();


                return log;



            }

        }
        public static async Task SaveSinvoiceLog(sinvoicelog log,string action)
        {
            try
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @action=action, @docentry = log.docentry, @errorCode = log.errorCode, @description = log.description, @supplierTaxCode = log.supplierTaxCode, @invoiceNo = log.invoiceNo, @transactionID = log.transactionID, @reservationCode = log.reservationCode, @objectjson = log.objectjson, @transtype = log.transtype, @originalinvoiceNo = log.originalinvoiceNo, @templatecode = log.templatecode, @serial = log.serial, @isupdatetax = log.isupdatetax, @iscanceled = log.iscanceled, @issuedate_unixtime = log.issuedate_unixtime, @issueDate = log.issueDate, @paymenttype = log.paymenttype, @ispayment = log.ispayment },
                        null, Int32.MaxValue, CommandType.StoredProcedure);
                    
                }
            }
            catch (Exception ex)
            {
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.ExecuteAsync(
                        "USP_SINVOICE_SAVE_LOG",
                        new { @action = action, @docentry = log.docentry, @errorCode = "444", @description = ex.Message.ToString(), @supplierTaxCode = log.supplierTaxCode, @invoiceNo = "", @transactionID = "", @reservationCode = "", @objectjson = log.objectjson, @transtype = log.transtype, @paymenttype = log.paymenttype, @ispayment = log.ispayment },
                        null, Int32.MaxValue, CommandType.StoredProcedure);
                }

            }

        }

        public static async Task<List<DocPublish>> GetListDocPublish(DateTime FromDate, DateTime ToDate,ILogger<Worker> _logger)
        {
            try
            {
              
                // Act
                using (var conn = new SqlConnection(connectstring))
                {
                    await conn.OpenAsync();
                    var result = await conn.QueryMultipleAsync(
                        "USP_SINVOICE_GET_DOC",
                        new { @FromDate = FromDate, @ToDate = ToDate },
                        null, Int32.MaxValue, CommandType.StoredProcedure);

                    List<DocPublish> docs = result.Read<DocPublish>().ToList();



                    return docs;



                }

            }
            catch (Exception ex)
            {

                _logger.LogError("Error Get List Doc Publish: {Message}", ex.Message);
                return null;
            }
            
        }

    }

}
